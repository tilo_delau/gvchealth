//
//  TableView1DataSource.swift
//  GVCHealth
//
//  Created by Tilo Delau on 2016-09-15.
//  Copyright © 2016 Tilo Delau. All rights reserved.
//

import UIKit

class TableView1DataSource: NSObject, UITableViewDataSource
{
    let tableView1Data = TableView1Data()
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableView1Data.checkBoxData().count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let data = tableView1Data.checkBoxData()

        print("table data: ", data)
        
        
        
        
        let cell: Cell1 = tableView.dequeueReusableCellWithIdentifier("cell1") as! Cell1
       // cell.labelLeft.text = data[indexPath.row][0]
        
        return cell
        /*
        let cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath)
        cell.textLabel?.text = str
        return cell
 */
    }
}
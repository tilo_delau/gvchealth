//
//  CheckBox.swift
//  GVCHealth
//
//  Created by Tilo Delau on 2016-09-14.
//  Copyright © 2016 Tilo Delau. All rights reserved.
//

import UIKit

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    class CheckBox: UIButton {
        
        //images
        let checkedImage = UIImage(named: "checked_checkbox")
        let unCheckedImage = UIImage(named: "unchecked_checkbox")
        
        //bool propety
        @IBInspectable var isChecked:Bool = false{
            didSet{
                self.updateImage()
            }
        }
        
        
        override func awakeFromNib() {
            self.addTarget(self, action: #selector(CheckBox.buttonClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            self.updateImage()
        }
        
        
        func updateImage() {
            if isChecked == true{
                self.setImage(checkedImage, forState: .Normal)
            }else{
                self.setImage(unCheckedImage, forState: .Normal)
            }
            
        }
        
        func buttonClicked(sender:UIButton) {
            if(sender == self){
                isChecked = !isChecked
            }
        }

}

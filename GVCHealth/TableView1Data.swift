//
//  TableView1Data.swift
//  GVCHealth
//
//  Created by Tilo Delau on 2016-09-15.
//  Copyright © 2016 Tilo Delau. All rights reserved.
//

import Foundation

class TableView1Data
{
    let checks = ["Nunc eu ullacorper", "Quisque eget odio", "Nulla facillsi", "Nunc eu ullacorper", "Quisque eget odio", "Nulla facillsi"]
    
    var checkData:[(name: String, value: Bool)] = [
        ("Nunc eu ullacorper", true),
        ("Nulla facillsi", false),
        ("Nunc eu ullacorper", false),
        ("Quisque eget odio", false),
        ("Nulla facillsi", false)
    ]
    
 func checkBoxData() -> [(name: String, value: Bool)]
    {
        return checkData
    }
    
    /*
    
    func rows() -> [String]
    {
        return checks
    }
 */
}
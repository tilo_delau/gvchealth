//
//  ButtonTableViewCell.swift
//  GVCHealth
//
//  Created by Tilo Delau on 2016-09-13.
//  Copyright © 2016 Tilo Delau. All rights reserved.
//

import UIKit

class Cell1: UITableViewCell {

    @IBOutlet weak var checkBoxLeft: CheckBox!
    @IBOutlet weak var labelLeft: UILabel!
    
    @IBOutlet weak var checkBoxRight: CheckBox!
    @IBOutlet weak var labelRight: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

class Cell2: UITableViewCell {
    
    @IBOutlet weak var checkBoxLeft: CheckBox!
    @IBOutlet weak var labelLeft: UILabel!

    @IBOutlet weak var checkBoxRight: CheckBox!
    @IBOutlet weak var labelRight: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}



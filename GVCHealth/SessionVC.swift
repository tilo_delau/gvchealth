//
//  SessionVC.swift
//  GVCHealth
//
//  Created by Tilo Delau on 2016-09-14.
//  Copyright © 2016 Tilo Delau. All rights reserved.
//

import UIKit
import PathMenu

class SessionVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView1: UITableView!
    @IBOutlet weak var tableView2: UITableView!
    
    let navBarTitle = "Anders Andersson"
    
    let dataSource1 = TableView1DataSource()
    
    let items = ["Ray Wenderlich", "NSHipster", "iOS Developer Tips", "Jameson Quave", "Natasha The Robot", "Coding Explorer", "That Thing In Swift", "Andrew Bancroft", "iAchieved.it", "Airspeed Velocity", "Ray Wenderlich", "NSHipster", "iOS Developer Tips", "Jameson Quave", "Natasha The Robot", "Coding Explorer", "That Thing In Swift", "Andrew Bancroft", "iAchieved.it", "Airspeed Velocity"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableView1.delegate    = self
        self.tableView1.dataSource  = dataSource1
        
        self.tableView2.delegate    = self
        self.tableView2.dataSource  = self
        
        self.title = navBarTitle
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        var numberOfSections = 0
        
        if tableView1 == self.tableView1 { numberOfSections = 3 }
        
        if tableView2 == self.tableView2 { numberOfSections = 2 }
        
        return numberOfSections
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var numberOfRows = 0
        
        if tableView1 == self.tableView1 { numberOfRows = 3 }
        
        if tableView2 == self.tableView2 { numberOfRows = 2 }
        
        return numberOfRows
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView1 == self.tableView1 {
            let cell: Cell1 = tableView1.dequeueReusableCellWithIdentifier("cell1") as! Cell1
            cell.labelLeft.text = "ej relevant"
            
            return cell
            
        } else {
        
            let cell: Cell2 = tableView2.dequeueReusableCellWithIdentifier("cell2") as! Cell2
            cell.labelRight.text = "ej relevant"
            
            return cell
            
        }
        
    }

}





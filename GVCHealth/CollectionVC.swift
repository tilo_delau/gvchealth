//
//  CollectionVC.swift
//  GVCHealth
//
//  Created by Tilo Delau on 2016-09-18.
//  Copyright © 2016 Tilo Delau. All rights reserved.
//

import UIKit

class CollectionVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    
    
    @IBOutlet weak var collectionView: UICollectionView!

    
    
    let checkListArr:[(name: String, value: Bool)] = [
        ("Nunc eu ullacorper", true),
        ("Nulla facillsi", false),
        ("Nunc eu ullacorper", false),
        ("Quisque eget odio", false),
        ("Nulla facillsi", false)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: Collection View
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(self.checkListArr.count)
        return self.checkListArr.count
    }
    
   
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("collectionCell", forIndexPath: indexPath) as! CollectionCell
        
        cell.cellLabel.text = self.checkListArr[indexPath.row].name
        cell.checkBox.selected = true
        
        return cell
        
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        print("Cell pressed: ", indexPath.row)
    }
    
    

}
